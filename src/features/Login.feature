Feature: Login

Scenario: User logs in
    Given I open the url "/"
    And I expect that element "//*[@type='email']" becomes displayed
    When I set "dancefront@gmail.com" to the inputfield "//*[@type='email']"
    And I set "te$t$tudent" to the inputfield "//*[@type='password']"
    And I click on the button "//button"
    Then I expect that element "//span[@class='name']" becomes displayed